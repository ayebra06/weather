from django.test import Client, TestCase

from .utils import mean


class TestView(TestCase):

    def test_index_view_no_params(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_view_just_lat_and_lon(self):
        c = Client()
        response = c.get('/12/23/')
        self.assertEqual(response.status_code, 200)

    def test_index_view_filters(self):
        c = Client()
        response = c.get('/13/42/accuweather/')
        self.assertEqual(response.status_code, 200)

    def test_index_view_multiple_filters(self):
        c = Client()
        response = c.get('/13/42/accuweather,noaa,weatherdotcom/')
        self.assertEqual(response.status_code, 200)


class TestMeanUtil(TestCase):

    def test_mean_simple(self):
        nums = [1, 2, 3]
        self.assertEqual(mean(nums), 2)


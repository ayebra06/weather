from django.http import JsonResponse

import requests

from .utils import mean


def index(request, latitude=None, longitude=None, sources=None):
    if not latitude or not longitude:
        return JsonResponse({'average temp':'no coordinates specified.'})
    if not sources:
        return JsonResponse({'average temp':'no coordinates or sources specified.'})

    temps = {'f': [], 'c': []}
    base_url = 'http://127.0.0.1:5000/'
    sources = sources.split(',')

    if 'accuweather' in sources:
        data = requests.get(base_url + 'accuweather?latitude={}&longitude={}'.format(latitude, longitude)).json()
        temps['f'].append(data['simpleforecast']['forecastday'][0]['high']['fahrenheit'])
        temps['f'].append(data['simpleforecast']['forecastday'][0]['low']['fahrenheit'])
        temps['c'].append(data['simpleforecast']['forecastday'][0]['high']['celsius'])
        temps['c'].append(data['simpleforecast']['forecastday'][0]['low']['celsius'])
    if 'noaa' in sources:
        data = requests.get(base_url + 'noaa?latlon={},{}'.format(latitude, longitude)).json()
        temps['f'].append(data['today']['high']['fahrenheit'])
        temps['f'].append(data['today']['low']['fahrenheit'])
        temps['c'].append(data['today']['high']['celsius'])
        temps['c'].append(data['today']['low']['celsius'])
    if 'weatherdotcom' in sources:
        data = requests.post(base_url + 'weatherdotcom', json={'lat': latitude, 'lon': longitude})
        units = data.json()['query']['results']['channel']['units']['temperature'].lower()
        temps[units].append(float(data.json()['query']['results']['channel']['condition']['temp']))

    return JsonResponse({
        'average_temps': {
            'celsius': mean([float(i) for i in temps['c']]),
            'fahrenheit': mean([float(i) for i in temps['f']])
        }
    })

# weather

Weather app

# Instructions

```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
./manage.py runserver
```

Make sure the flask application from https://github.com/shipwell/mock-weather-api is running in another process.


Get average tempature by entering lat and long parameters followed by comma separated sources like the following examples
- http://localhost:8000/33.3/4.44/noaa/
- http://localhost:8000/33.3/4.44/noaa,accuweather/
- http://localhost:8000/33.3/4.44/accuweather,noaa/


###Testing
```
./manage.py test
```

* Note: If you get and error such as `requests.exceptions.ConnectionError:` Make sure your flask app is running.
